﻿





CREATE      
PROCEDURE [ETL].[ProcImport_LoadCustomerImports_HamiltonMembers]
AS
SET NOCOUNT ON
/*********************************************************************
--* ======================================================================
--*  Author: Gene Kirkpatrick 
--*  Date Created: 9/20/2021
--*  VSS: 
--*=======================================================================
*
*	
*
*	Description: Import Customers Data from AS400 file. Data is imported into [ETL].[Hamilton_Members] table
                 Using SSIS.
 .
*			
*	PARAMETERS: 
*
*	Change Log (Date, Name and Ddescription)
*
*	--------------------------------------------------------
*	Modification History: 
*********************************************************************/

update [ETL].[Hamilton_Members]
set [WMDOB] =1900-01-01
where cast(left([WMDOB],10)as date) < cast('1800-01-01' as date)
  OR Cast(left([WMDOB],10)as date) > cast('9999-01-01' as date);


  TRUNCATE  TABLE [ETL].CustomerImport;

 
DECLARE 
	@RowCount VARCHAR(255) = ''
	


BEGIN TRY
    BEGIN TRANSACTION

INSERT INTO [ETL].[CustomerImport] ([CustomerImportID],[Description],[ImportedID],[BirthDate],[IsVerified]
             ,[CustomerNumber] ,[ProviderCustomerNumber] ,[Gender],[MedicalNumber],[Address1],[Address2]
             ,[City],[State],[Zip],[Latitude],[Longitude],[Cell Phone],[Home Phone],[Work Phone],[Email]
             ,[Language],[optInNightlyCallout],[OptInEmailNotifications],[optInVoiceNotifications]  
             ,[optinTextNotifications],[optinGeneralCallout],[OpDefaultResPassengerType],[OpDefaultResFundingType]
             ,[DefaultResCompanions],[OpDefaultResService],[OpDefaultResFareType],[OpDefaultResOverrideType]        
             ,[First_Name],[Last_Name],[Extended_Info],[SSN])   
           
     
	 
	  SELECT 
	   [CustomerImportID]           =   [WMEDNUM]
      ,[Description]				=  CONCAT([WMLAST],', ',[WMFIRST])
      ,[ImportedID]					=  6
      ,[BirthDate]					= ISNULL([WMDOB],'1/1/1900')
      ,[IsVerified]					= 0
      ,[CustomerNumber]				= CONCAT(Left(LTRIM(UPPER([WMFIRST])),2),Left(ltrim(UPPER([WMLAST])),2),Right(rtrim(WMEDNUM),5))

      ,[ProviderCustomerNumber]		=  NULL
      ,[Gender]					    = 'N/A'    
      ,[MedicalNumber]				=  '' 
      ,[Address1]					= [WAADDR2]
      ,[Address2]					= ''
      ,[City]						= [WACITY]
      ,[State]						= [WASTATE]
      ,[Zip]						= [WAZIP]
      ,[Latitude]				    = CASE WHEN [WALAT] = '.000000000000000' THEN '0.0' ELSE [WALAT] END
      ,[Longitude]				    = CASE WHEN [WALON] = '.000000000000000' THEN '0.0' ELSE [WALON] END
      ,[Cell Phone]					= [WMCELL]
      ,[Home Phone]					= [WMPHONE]
      ,[Work Phone]					= [WMWORK]
      ,[Email]						= ''
      ,[Language]					= 'English'     --'Other' 
      ,[optInNightlyCallout]        = 0 --CASE WHEN [EnableVoice] Like 'Y%' THEN 1 ELSE 0  END  
	  ,[OptInEmailNotifications]	= 0
	  ,[optInVoiceNotifications]    = 0 --CASE WHEN [EnableVoice] Like 'Y%' THEN 1 ELSE 0  END                                 
	  ,[optinTextNotifications]     = 0
	  ,[optinGeneralCallout]		= 0 --CASE WHEN [EnableVoice] Like 'Y%' THEN 1 ELSE 0  END  
	  ,[OpDefaultResPassengerType]  = NULL
	  ,[OpDefaultResFundingType]	= 'N/A'
	  ,[DefaultResCompanions]       = 0 
	  ,[OpDefaultResService]        = 'MTM'
      ,[OpDefaultResFareType]       = 'N/A'
      ,[OpDefaultResOverrideType]   = 'N/A'
	  ,[First_Name]                 = [WMFIRST]  
	  ,[Last_Name] 					= [WMLAST]
	  ,[Extended_Info]              = NULL 
	  ,[SSN]                       =  [WMEDNUM]
FROM [ETL].[Hamilton_Members]
where [WASTATE] is Not null

  -- Set @RowCount for the last insert statement 
              SET @RowCount = CONCAT(@RowCount, ', [ETL].[CustomerImport] : ',  CAST(@@RowCount AS VARCHAR(255)));




SELECT @RowCount,0,NULL; 

 COMMIT TRANSACTION
END TRY 
BEGIN CATCH
	IF @@ERROR > 0
	 ROLLBACK TRANSACTION;

	SELECT NULL,-1, CONCAT('ErrorLine: ',ERROR_LINE(),', Error: ',ERROR_MESSAGE());

END CATCH;