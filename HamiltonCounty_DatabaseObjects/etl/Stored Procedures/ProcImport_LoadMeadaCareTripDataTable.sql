﻿----------------------------------------------------------------------------------------------------------------------------------------------

CREATE                
PROCEDURE [etl].[ProcImport_LoadMeadaCareTripDataTable]
AS
SET NOCOUNT ON
/*********************************************************************
--* ======================================================================
--*  Author: Gene Kirkpatrick 
--*  Date Created: 9/30/2021
--*  VSS: 
--*=======================================================================
*
*	File Name:	
*
*	Description: Import Trips Data from de.Hamilton_Trips_Raw table. 
 .
*			
*	PARAMETERS: 
*
*	Change Log (Date, Name and Ddescription)
*
*	--------------------------------------------------------
*	Modification History: 
*********************************************************************/


UPDATE MTD
     SET  
           [TripCategory]		  = RTRIM(LTRIM(HT.TRIP_CATEGORY))
          ,[PickupProvider]		  = RTRIM(LTRIM(HT.PICKUP_PROVIDER))
          ,[DropoffProvider]	  = RTRIM(LTRIM(HT.DROPOFF_PROVIDER))
          ,[MobilityAide]		  = RTRIM(LTRIM(HT.MOBILITY_AIDE))
          ,[Monitor]			  = RTRIM(LTRIM(HT.MONITOR))
          ,[AuthLastName]		  = RTRIM(LTRIM(HT.AUTHORIZED_REP))
          ,[Comments]			  = RTRIM(LTRIM(HT.COMMENTS))
          ,[ConsentToTransport]   = RTRIM(LTRIM(HT.DROPOFF_CONSENT))
		  ,[Last_Update_date]     = ISNULL(NULLIF(RTRIM(LTRIM(HT.LAST_UPDATE_DATE)),''),'0001-01-01')
		  ,[StatusDescription]     = TRIP_STATUS

	FROM [de].[MedaCareTripData] MTD
	INNER JOIN [ETL].[Hamilton_Trips_Raw] HT ON Concat(Ltrim(rtrim(HT.[TRIP_ID])),'   ',Rtrim(Ltrim(HT.[TRIP_LEG_ID]))) = mtd.ProviderTripID;



	INSERT INTO [de].[MedaCareTripData]  (  [ProviderTripID],[TripCategory] ,[PickupProvider],[DropoffProvider]
                                           ,[MobilityAide] ,[Monitor],[AuthLastName] ,[Comments] ,[ConsentToTransport],[Last_Update_date],[StatusDescription])
                   
               
	SELECT
	       [ProviderTripID]       = Concat(Ltrim(rtrim(HT.[TRIP_ID])),'   ',Rtrim(Ltrim(HT.[TRIP_LEG_ID])))
	      ,[TripCategory]		  = LTRIM(RTrim(HT.TRIP_CATEGORY))
          ,[PickupProvider]		  =LTRIM(RTRIM(HT.PICKUP_PROVIDER))
          ,[DropoffProvider]	  =LTRIM(RTRIM(HT.DROPOFF_PROVIDER))
          ,[MobilityAide]		  =LTRIM(RTRIM(HT.MOBILITY_AIDE))
          ,[Monitor]			  =LTRIM(RTRIM(HT.MONITOR))
          ,[AuthLastName]		  =LTRIM(RTRIM(HT.AUTHORIZED_REP))
          ,[Comments]			  =LTRIM(RTRIM(HT.COMMENTS))
          ,[ConsentToTransport]   =LTRIM(RTRIM(HT.DROPOFF_CONSENT))
	     ,[Last_Update_date]      = ISNULL(NULLIF(RTRIM(LTRIM(HT.LAST_UPDATE_DATE)),''),'0001-01-01')
		 ,[StatusDescription]     = TRIP_STATUS
	FROM [ETL].[Hamilton_Trips_Raw] HT
	LEFT JOIN [de].[MedaCareTripData] MTD ON Concat(Ltrim(rtrim(HT.[TRIP_ID])),'   ',Rtrim(Ltrim(HT.[TRIP_LEG_ID]))) = mtd.ProviderTripID
	WHERE MTD.[ProviderTripID]  IS NULL;
GO


