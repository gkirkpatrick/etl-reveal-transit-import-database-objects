﻿

CREATE      
PROCEDURE [ETL].[ProcImport_Customers]
AS
SET NOCOUNT ON
/*********************************************************************
--* ======================================================================
--*  Author: Gene Kirkpatrick 
--*  Date Created: 6/2/2021
--*  VSS: 
--*=======================================================================
*
*	File Name:	Customers
*
*	Description: Import Customers Data from ETL.ImportCustomers table. 
 .
*			
*	PARAMETERS: 
*
*	Change Log (Date, Name and Ddescription)
*
*	--------------------------------------------------------
*	Modification History: 
*********************************************************************/

DECLARE 
      @RowCount	 VARCHAR(255) = ''
		



BEGIN TRY
    BEGIN TRANSACTION


	--Mapping Table for CUSTOMER being Imported with Current Reveal Data
	PRINT 'Create Mapping Table: ' + CONVERT(VARCHAR(30), GETDATE(), 121)
	DECLARE @ImportMapping TABLE
	(
		CustomerImportID INT,	
		CustomerID INT NULL,
		GenderID INT NULL,
		[OpDefaultResPassengerTypeID] INT NULL,
		HomeLocationID INT NULL	,
		[OpDefaultResFundingTypeID] INT NULL,
        [OpDefaultResService] INT NULL,
		[OpDefaultResOverrideType]INT NULL,
		[OpDefaultResFareType]INT NULL

	)
	

---------------------

	INSERT INTO @ImportMapping
		SELECT
			C.CustomerImportID,		
			de.Import_GetCustomerID(C.Description, C.BirthDate, C.CustomerNumber) AS [CustomerID],
			de.Import_GetGenderID(C.Gender) AS [GenderID],
			de.Import_GetPassengerTypeID(C.OpDefaultResPassengerType) AS [OpDefaultResPassengerTypeID],
			de.Import_GetLocationID(''/*PickCommonName*/, C.Address1, C.Address2, C.City, C.[State], C.[Zip], C.Latitude, C.[Longitude]) AS [HomeLocationID],			
			de.Import_GetFundingTypeID(C.OpDefaultResFundingType) AS [OpDefaultResFundingType],
           [de].[Import_GetServiceID](c.[OpDefaultResService])AS [OpDefaultResService],
           [de].[Import_GetOverrideTypeID](C.[OpDefaultResOverrideType]) AS[OpDefaultResOverrideType],
		   [de].[Import_GetFareTypeID](c.[OpDefaultResFareType]) as [OpDefaultResFareType]
		
		FROM ETL.CustomerImport C 
	--select * from @ImportMapping
	-- Select distinct OpDefaultResFundingType from ETL.CustomerImport where de.Import_GetFundingTypeID(OpDefaultResFundingType) IS NULL


	
	-- IMPORT LOCATIONS
	PRINT 'Locations: ' + CONVERT(VARCHAR(30), GETDATE(), 121)
	INSERT INTO Locations  ([Description], ImportDescription, Address1, Address2, City, [State], Zip, Latitude, Longitude, OverrideImport, Active,
							CurrentLocationID, UserID, CreatedTime_UTC, GeoCodeTypeID, ManifestNote, CommonName)
		--These two are both the same, Description AND ImportDescription get the same text to start with

		SELECT
			CASE 
				WHEN nl.CommonName != '' 
					THEN '['+nl.CommonName+'] '
				ELSE ''
			END + 
			nl.Address1 + ' ' + 
			CASE
				WHEN nl.Address2 != ''
					THEN nl.Address2 + ' '
				ELSE ''
			END +
			nl.City + ', ' + nl.[State] + ' ' + nl.Zip AS [Description],

			CASE 
				WHEN nl.CommonName != '' 
					THEN '['+nl.CommonName+'] '
				ELSE ''
			END + 
			nl.Address1 + ' ' + 
			CASE
				WHEN nl.Address2 != ''
					THEN nl.Address2 + ' '
				ELSE ''
			END +
			nl.City + ', ' + nl.[State] + ' ' + nl.Zip AS [ImportDescription],

			nl.Address1 AS [Address1],
			nl.Address2 AS [Address2],
			nl.City,
			nl.[State],
			nl.Zip,
			nl.Latitude,
			nl.Longitude,
			1 AS [OverrideImport],
			1 AS [Active],
			0 AS [CurrentLocationID],
			6 AS UserID,
			GETUTCDATE() AS [CreatedTime_UTC],
			CASE WHEN nl.Latitude = 0 THEN 4 ELSE 6 END AS [GeoCodeTypeID],			
			nl.Note,
			nl.CommonName
		FROM (
				--Home Location
				SELECT DISTINCT 
					 [CommonName] = ''
					,[Address1] = C.Address1
					,[Address2] = C.Address2
					,[City]     = C.City
					,[State]    = C.State
					,[Zip]      = C.Zip			
					,[Latitude] = C.Latitude 
					,[Longitude]= C.Longitude
		            ,[Note]     = 'ETLCustomerImport'
				FROM  ETL.CustomerImport C INNER JOIN @ImportMapping im ON im.CustomerImportID = C.CustomerImportID
				WHERE im.HomeLocationID IS NULL
				) NL

  SET @RowCount = CONCAT(@RowCount,'Locations: ', CAST(@@ROWCOUNT AS VARCHAR));

--IMPORT CUSTOMERS
	Print 'Insert New Customers: ' + Convert(VARCHAR(30), GetDate(), 121)
	INSERT INTO dbo.Customers ([Description],[Active],[ImportedID], [BirthDate],[IsVerified],[CustomerNumber]
	                          ,[ProviderCustomerNumber],[GenderID],[MedicalNumber],[HomeLocationID],[MailLocationID],[DefaultResLocationID],[EmailAddress],[optInNightlyCallout],
							  [OptInEmailNotifications],[OptInVoiceNotifications],[OptInTextNotifications],[OptInGeneralCallout]
							  ,[OpDefaultResPassengerTypeID],[OpDefaultResFundingTypeID],[VoiceNotificationNumber],[TextNotificationNumber],[LanguageID] ,[OpDefaultResServiceID]  
							  ,[OpDefaultResFareTypeID] ,[OpDefaultResOverrideTypeID],[FirstName] ,[LastName] ,[DefaultResCompanions] ,[DefaultResAttendants],[Note]   )
																																								
SELECT DISTINCT        		   																												
       [Description]			    =	 C.[Description]
	  ,[Active]			      	    =     1
      ,[ImportedID]			    	=	 C.[ImportedID]					
      ,[BirthDate]				    =	 C.[BirthDate]					
      ,[IsVerified]					=	 C.[IsVerified]					
      ,[CustomerNumber]				=	 C.[CustomerNumber]				
      ,[ProviderCustomerNumber]		=	 C.[ProviderCustomerNumber]		
      ,[GenderID]				    =    IM.[GenderID]					    
      ,[MedicalNumber]				=	 C.[MedicalNumber]				
      ,[HomeLocationID]		        = ISNULL( de.Import_GetLocationID(''/*PickCommonName*/, C.Address1, C.Address2, C.City, C.[State], C.[Zip], C.Latitude, C.[Longitude]),0)   				
      ,[MailLocationID]             = ISNULL(de.Import_GetLocationID(''/*PickCommonName*/, C.Address1, C.Address2, C.City, C.[State], C.[Zip], C.Latitude, C.[Longitude]),0) 
	  ,[DefaultResLocationID]       = ISNULL(de.Import_GetLocationID(''/*PickCommonName*/, C.Address1, C.Address2, C.City, C.[State], C.[Zip], C.Latitude, C.[Longitude]),0) 
	 -- ,[Home Phone]					=	 C.[Home Phone]					
     -- ,[Work Phone]					=	 C.[Work Phone]					
      ,[Emailaddress]				=	 C.[Email]						
		
      ,[optInNightlyCallout]        =    C.[optInNightlyCallout]  
	  ,[OptInEmailNotifications]	=	 C.[OptInEmailNotifications]	
	  ,[optInVoiceNotifications]    =    C.[optInVoiceNotifications]                              
	  ,[optinTextNotifications]     =	 C.[optinTextNotifications]     
	  ,[optinGeneralCallout]		=	 C.[optinGeneralCallout]		
	  ,[OpDefaultResPassengerTypeID]=	 ISNULL(IM.[OpDefaultResPassengerTypeID],1)  
	  ,[OpDefaultResFundingTypeID]	=	 IM.[OpDefaultResFundingTypeID]	
	  ,[VoiceNotificationNumber]      =  CASE WHEN [optInVoiceNotifications] = 1 AND [Cell Phone] IS NOT NULL OR [Home Phone] IS NOT NULL
	                                           THEN LEFT(ISNULL(NULLIF(RTRIM(LTRIM(Replace(Replace(replace(replace([Cell Phone],'-',''),'(',''),')',''),' ','') )),''),RTRIM(LTRIM(Replace(Replace(replace(Replace([Home Phone],'-',''),'(',''),')',''),' ','') ))),10)   -- add for conviva remove all "-" ,")", '('
											    ELSE '' END 
	  ,[TextNotificationNumber]       = '' /*CASE WHEN C.[optInVoiceNotifications] = 1 
	                                           THEN ISNULL(replace(C.[Cell Phone],'-',''),Replace(C.[Home Phone],'-',''))   -- add for conviva
											    ELSE '' END */

	  ,[LanguageID]                  = CASE WHEN c.[Language] LIKE 'English' THEN 1    -- NEED TO add to process
	                                       WHEN c.[Language] LIKE 'Spanish' THEN 2
										   WHEN c.[Language] LIKE 'Creole' THEN 3
										  
										   ELSE 1 END
    ,[OpDefaultResServiceID]          = im.[OpDefaultResService]     
	,[OpDefaultResFareTypeID]		   = im.[OpDefaultResFareType]		
	,[OpDefaultResOverrideTypeID]	   = im.[OpDefaultResOverrideType]
	,[FirstName]                       = C.[First_Name]      
	,[LastName]                        = C.[Last_Name]   
	,[DefaultResCompanions]            = C.DefaultResCompanions
	,[DefaultResAttendants]            = c.DefaultResAttendants
	,[Note]                            = C.note

 FROM  ETL.CustomerImport C INNER JOIN @ImportMapping im ON im.CustomerImportID = C.CustomerImportID
WHERE im.CustomerID IS NULL	

SET @RowCount = CONCAT(@RowCount,'Insert New Customers: ', CAST(@@ROWCOUNT AS VARCHAR));

---- Update


--IMPORT CUSTOMERS
	Print 'Update Customers: ' + Convert(VARCHAR(30), GetDate(), 121)
	UPDATE dbo.Customers               
	   SET [Description]				= Cu.[Description]				
          ,[Active]						= Cu.[Active]						
          ,[ImportedID]					= Cu.[ImportedID]					
          ,[BirthDate]					= Cu.[BirthDate]					
		  ,[IsVerified]					= Cu.[IsVerified]					
		  ,[CustomerNumber]				= Cu.[CustomerNumber]				
	      ,[ProviderCustomerNumber]		= Cu.[ProviderCustomerNumber]		
		  ,[GenderID]					= Cu.[GenderID]					
		  ,[MedicalNumber]				= Cu.[MedicalNumber]				
		  ,[HomeLocationID]             = ISNULL(Cu.[HomeLocationID],0)				
		  ,[MailLocationID]				= ISNULL(Cu.[MailLocationID],0)				
		  ,[EmailAddress]				= Cu.[EmailAddress]				
		  ,[OptInEmailNotifications]	= Cu.[OptInEmailNotifications]	
		  ,[OptInVoiceNotifications]	= Cu.[OptInVoiceNotifications]	
		  ,[OptInTextNotifications]		= Cu.[OptInTextNotifications]		
		  ,[OptInGeneralCallout]		= Cu.[OptInGeneralCallout]		
          ,[OpDefaultResPassengerTypeID]= ISNULL(Cu.[OpDefaultResPassengerTypeID],0)
		  ,[OpDefaultResFundingTypeID]	= Cu.[OpDefaultResFundingTypeID]	
	      ,[VoiceNotificationNumber]	= Cu.[VoiceNotificationNumber]	
		--  ,[TextNotificationNumber]		= Cu.[TextNotificationNumber]		
		  ,[LanguageID]  				= Cu.[LanguageID]  				
		  ,[FirstName]                  = CU.[FirstName] 
	      ,[LastName]                   = CU.[LastName] 
		  ,[DefaultResCompanions]       = CU.[DefaultResCompanions]  
FROM (
SELECT [CustomerID]                 =    im.CustomerID        		   
      ,[Description]			    =	 C.[Description]
	  ,[Active]			      	    =     1
      ,[ImportedID]			    	=	 C.[ImportedID]					
      ,[BirthDate]				    =	 C.[BirthDate]					
      ,[IsVerified]					=	 C.[IsVerified]					
      ,[CustomerNumber]				=	 C.[CustomerNumber]				
      ,[ProviderCustomerNumber]		=	 C.[ProviderCustomerNumber]		
      ,[GenderID]				    =    IM.[GenderID]					    
      ,[MedicalNumber]				=	 C.[MedicalNumber]				
      ,[HomeLocationID]		        =  de.Import_GetLocationID(''/*PickCommonName*/, C.Address1, C.Address2, C.City, C.[State], C.[Zip], C.Latitude, C.[Longitude])   				
      ,[MailLocationID]             = de.Import_GetLocationID(''/*PickCommonName*/, C.Address1, C.Address2, C.City, C.[State], C.[Zip], C.Latitude, C.[Longitude]) 
	 -- ,[Home Phone]					=	 C.[Home Phone]					
     -- ,[Work Phone]					=	 C.[Work Phone]					
      ,[Emailaddress]				=	 C.[Email]						
   --   ,[LanguageID]					=	 C.[Language]		        Need to create funcation for LanguageId			
      ,[OptInEmailNotifications]	=	 C.[OptInEmailNotifications]	
	  ,[optInVoiceNotifications]    =    C.[optInVoiceNotifications]                              
	  ,[optinTextNotifications]     =	 C.[optinTextNotifications]     
	  ,[optinGeneralCallout]		=	 C.[optinGeneralCallout]		
	  ,[OpDefaultResPassengerTypeID]=	 IM.[OpDefaultResPassengerTypeID]  
	  ,[OpDefaultResFundingTypeID]	=	 IM.[OpDefaultResFundingTypeID]	
	  ,[VoiceNotificationNumber]     =  CASE WHEN [optInVoiceNotifications] = 1  AND [Cell Phone] IS NOT NULL OR [Home Phone] IS NOT NULL
	                                           THEN LEFT(ISNULL(NULLIF(RTRIM(LTRIM(Replace(Replace(replace(replace([Cell Phone],'-',''),'(',''),')',''),' ','') )),''),RTRIM(LTRIM(Replace(Replace(replace(Replace([Home Phone],'-',''),'(',''),')',''),' ','') ))),10)   -- add for conviva remove all "-" ,")", '('
											    ELSE '' END 
	 /* ,[TextNotificationNumber]       =   CASE WHEN C.[optInVoiceNotifications] = 1 
	                                           THEN ISNULL(NULLIF(replace(C.[Cell Phone],'-',''),''),Replace(C.[Home Phone],'-',''))   -- add for conviva remove all "-" ,")", '('
											    ELSE '' END */

	  ,[LanguageID]                  = CASE WHEN c.[Language] LIKE 'English' THEN 1    -- NEED TO add to process
	                                       WHEN c.[Language] LIKE 'Spanish' THEN 2
										   WHEN c.[Language] LIKE 'Creole' THEN 3
										    ELSE 4 END
   ,[FirstName]                      = C.[First_Name]            
   ,[LastName]                       = C.[Last_Name]   
   ,[DefaultResCompanions]           = C.[DefaultResCompanions]  

 FROM  ETL.CustomerImport C INNER JOIN @ImportMapping im ON im.CustomerImportID = C.CustomerImportID
WHERE im.CustomerID IS NOT NULL	
) CU
WHERE CU.CustomerID = dbo.[Customers].[CustomerID]

SET @RowCount = CONCAT(@RowCount,'Update Customers: ', CAST(@@ROWCOUNT AS VARCHAR));

/*
PRINT 'Update Existing Customers: ' + CONVERT(VARCHAR(30), GETDATE(), 121)
	UPDATE Customers SET GenderID = im.GenderID
	FROM Customers c INNER JOIN @ImportMapping im ON im.CustomerID = c.CustomerID
*/
	
	--Update Customer Mapping for Contacts
	PRINT 'Update Mapping with new Customers: ' + CONVERT(VARCHAR(30), GETDATE(), 121)
	UPDATE @ImportMapping SET CustomerID = de.Import_GetCustomerID(c.Description, c.BirthDate, c.CustomerNumber)
	FROM @ImportMapping im INNER JOIN ETL.CustomerImport C ON C.CustomerImportID = im.CustomerImportID
	
	

--CUSTOMER CONTACTS ** NEW PRIMARY
	Print 'Insert New Primary Customer Contacts; ' + Convert(VARCHAR(30), GetDate(), 121)
	INSERT INTO CustomerContacts (CustomerContactTypeID, CustomerID, [Order], [Description], Active, Created_UTC)
		SELECT DISTINCT 
			CASE WHEN [Home Phone]IS NOT NULL THEN 2 --Home Phone
			     WHEN [Home Phone] IS NULL AND [Cell Phone] IS NOT NULL THEN 1 -- CEll Phone
				 WHEN [Home Phone] IS NULL AND [Cell Phone] IS NULL AND [Work Phone] IS NOT NULL THEN 3 -- work
				  ELSE NULL END AS [CustomerContactTypeID], 

			im.CustomerID AS [CustomerID],
			1 AS [Order], --Primary contact

			CASE WHEN [Home Phone]IS NOT NULL THEN [Home Phone] --Home Phone
			     WHEN [Home Phone] IS NULL AND [Cell Phone] IS NOT NULL THEN [Cell Phone] -- CEll Phone
				 WHEN [Home Phone] IS NULL AND [Cell Phone] IS NULL AND [Work Phone]  IS NOT NULL THEN [Work Phone] 
				  ELSE NULL  END AS [Description],

			1 AS [Active],
			GETUTCDATE() AS [Created_UTC]
		FROM ETL.CustomerImport C INNER JOIN @ImportMapping im ON im.CustomerImportID = C.CustomerImportID
		WHERE 
			NOT EXISTS (SELECT 1 FROM CustomerContacts icc WHERE icc.CustomerID = im.CustomerID AND [Order] = 1 AND Active = 1) 
			AND im.CustomerID IS NOT NULL
		    AND (C.[HOME Phone] IS NOT NULL OR C.[Cell Phone] IS NOT NULL OR C.[Work Phone] IS NOT NULL)
 

SET @RowCount = CONCAT(@RowCount,'New Primary Customer Contacts: ', CAST(@@ROWCOUNT AS VARCHAR));


--CUSTOMER CONTACTS **SECONDARY CONTACTS
	Print 'Insert New Secondary Customer Contacts; ' + Convert(VARCHAR(30), GetDate(), 121)
	INSERT INTO CustomerContacts (CustomerContactTypeID, CustomerID, [Order], [Description], Active, Created_UTC)
		SELECT DISTINCT 
			CASE WHEN [Home Phone] IS NOT NULL AND [Cell Phone] IS NOT NULL THEN 1 --cell Phone
			     WHEN [Home Phone] IS NULL AND [Cell Phone] IS NOT NULL AND [Work Phone] IS NOT NULL THEN 3 -- Work Phone
				 WHEN [Home Phone] IS NOT NULL AND [CELL PHONE] IS NULL AND [Work Phone] IS NOT NULL THEN 3
				  ELSE NULL END AS [CustomerContactTypeID],

			im.CustomerID AS [CustomerID],
			2 AS [Order],   -- Secondary Contact

			CASE WHEN [Home Phone] IS NOT NULL AND [Cell Phone] IS NOT NULL THEN  [Cell Phone] --cell Phone
			     WHEN [Home Phone] IS NULL AND [Cell Phone] IS NOT NULL THEN [Work Phone] -- Work Phone
				 WHEN[Home Phone] IS NOT NULL AND [Cell Phone] IS NULL THEN [Work Phone] --work ph
				  ELSE NULL END AS [Description],

			1 AS [Active],
			GETUTCDATE() AS [Created_UTC]
		FROM ETL.CustomerImport C INNER JOIN @ImportMapping im ON im.CustomerImportID = C.CustomerImportID
		WHERE 
			NOT EXISTS (SELECT 1 FROM CustomerContacts icc WHERE icc.CustomerID = im.CustomerID AND [Order] = 2 AND Active = 1) 
			AND im.CustomerID IS NOT NULL
			AND( (C.[Home Phone]  IS NOT NULL AND (C.[Cell Phone]  IS NOT NULL OR C.[Work Phone] IS NOT NULL)) 
			      OR ( C.[Home Phone] IS NULL AND (C.[Cell Phone] IS NOT NULL AND C.[Work Phone] IS NOT NULL))
				     
				) 
				   
  SET @RowCount = CONCAT(@RowCount,'New Secondary Customer Contacts: ', CAST(@@ROWCOUNT AS VARCHAR));

--CUSTOMER CONTACTS **THIRD CUSTOMER CONTACTS
	Print 'Insert New Third Customer Contacts; ' + Convert(VARCHAR(30), GetDate(), 121)
	INSERT INTO CustomerContacts (CustomerContactTypeID, CustomerID, [Order], [Description], Active, Created_UTC)
		SELECT DISTINCT 
			3 AS [CustomerContactTypeID], --Work Phone
			im.CustomerID AS [CustomerID],
			3 AS [Order], --Third contact
			CASE WHEN C.[Home Phone] IS NOT NULL AND C.[Cell phone] IS NOT NULL AND C.[Work Phone]  IS NOT NULL THEN C.[Work Phone] ELSE NULL END AS [Description],
			1 AS [Active],
			GETUTCDATE() AS [Created_UTC]
		FROM ETL.CustomerImport C INNER JOIN @ImportMapping im ON im.CustomerImportID = C.CustomerImportID
		WHERE 
			NOT EXISTS (SELECT 1 FROM CustomerContacts icc WHERE icc.CustomerID = im.CustomerID AND [Order] = 3 AND Active = 1) 
			AND im.CustomerID IS NOT NULL
			AND (C.[Home Phone] IS NOT NULL AND C.[Cell phone] IS NOT NULL AND C.[Work Phone]  IS NOT NULL)

  SET @RowCount = CONCAT(@RowCount,'New Third Customer Contacts: ', CAST(@@ROWCOUNT AS VARCHAR));


-- EXTENDED INFO

   Print 'Insert CustomerSystemTripNotes (ExtendedInfo); ' + Convert(VARCHAR(30), GetDate(), 121)
	INSERT INTO  [dbo].[CustomerSystemTripNotes]  ([CustomerID] ,[SystemTripNoteID] ,[Active],[Created_UTC]
                                              ,[CreatedUserID] ,[Deleted_UTC],[DeletedUserID])

        SELECT  
               [CustomerID]           = [de].[Import_GetCustomerID] ([Description],[BirthDate],[CustomerNumber])
              ,[SystemTripNoteID]     = [de].[Import_GetSystemTripNoteID](Extended_Info)
              ,[Active]               = 1 
              ,[Created_UTC]          = GETUTCDATE()
              ,[CreatedUserID]        = 6
              ,[Deleted_UTC]          = NULL
              ,[DeletedUserID]		  = NULL
          FROM ETL.CustomerImport
          WHERE Extended_Info  <> '' and [de].[Import_GetSystemTripNoteID](Extended_Info) IS NOT NULL

 
  SET @RowCount = CONCAT(@RowCount,'Insert CustomerSystemTripNotes (ExtendedInfo): ', CAST(@@ROWCOUNT AS VARCHAR));


---  Setup Customer Default HomeLoction
Print 'Insert CustomerCommonLocations; ' + Convert(VARCHAR(30), GetDate(), 121)
INSERT INTO CustomerCommonLocations (CustomerID,LocationID,Active,Created_UTC,CreatedUserID)

SELECT   CustomerID 
        ,LocationID = [DefaultResLocationID]
		, Active = 1
		,GETUTCDATE()
		,CreateduserID = 6
  
 FROM Customers 
WHERE CustomerID IN(select de.Import_GetCustomerID(C.Description, C.BirthDate, C.CustomerNumber) AS [CustomerID] from ETL.CustomerImport C)

  SET @RowCount = CONCAT(@RowCount,'New CustomerCommonLocations (Default HomeLoction): ', CAST(@@ROWCOUNT AS VARCHAR));
 


SELECT @RowCount,0,NULL; 

 COMMIT TRANSACTION
END TRY 
BEGIN CATCH
	IF @@ERROR > 0
	 ROLLBACK TRANSACTION;

	SELECT NULL,-1, CONCAT('ErrorLine: ',ERROR_LINE(),', Error: ',ERROR_MESSAGE());

END CATCH;