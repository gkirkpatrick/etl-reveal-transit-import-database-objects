﻿


CREATE        
PROCEDURE ETL.ProcImport_Location
AS
SET NOCOUNT ON

/*********************************************************************
--* ======================================================================
--*  Author: Gene Kirkpatrick 
--*  Date Created: 6/3/2021
--*  VSS: 
--*=======================================================================
*
*	File Name:	Locations
*
*	Description: Created to import locations from de.LocationImport Table
 .
*			
*	PARAMETERS: 
*
*	Change Log (Date, Name and Ddescription)
*
*	--------------------------------------------------------
*	Modification History: 
*********************************************************************/

DECLARE 
      @RowCount	 VARCHAR(255) = ''
		



BEGIN TRY
    BEGIN TRANSACTION

	
	-- IMPORT LOCATIONS
	PRINT 'Locations: ' + CONVERT(VARCHAR(30), GETDATE(), 121)
	INSERT INTO Locations  ([Description], ImportDescription, Address1, Address2, City, [State], Zip, Latitude, Longitude, OverrideImport, Active,
							CurrentLocationID, UserID, CreatedTime_UTC, GeoCodeTypeID, Notes /*Changed from mfnotes for conviva*/, CommonName)
		--These two are both the same, Description AND ImportDescription get the same text to start with

		SELECT
			CASE 
				WHEN nl.CommonName != '' 
					THEN '['+nl.CommonName+'] '
				ELSE ''
			END + 
			nl.Address1 + ' ' + 
			CASE
				WHEN nl.Address2 != ''
					THEN nl.Address2 + ' '
				ELSE ''
			END +
			nl.City + ', ' + nl.[State] + ' ' + nl.Zip AS [Description],

			CASE 
				WHEN nl.CommonName != '' 
					THEN '['+nl.CommonName+'] '
				ELSE ''
			END + 
			nl.Address1 + ' ' + 
			CASE
				WHEN nl.Address2 != ''
					THEN nl.Address2 + ' '
				ELSE ''
			END +
			nl.City + ', ' + nl.[State] + ' ' + nl.Zip AS [ImportDescription],

			nl.Address1 AS [Address1],
			nl.Address2 AS [Address2],
			nl.City,
			nl.[State],
			nl.Zip,
			nl.Latitude,
			nl.Longitude,
			1 AS [OverrideImport],
			1 AS [Active],
			0 AS [CurrentLocationID],
			6 AS UserID,
			GETUTCDATE() AS [CreatedTime_UTC],
			CASE WHEN nl.Latitude = 0 THEN 4 ELSE 6 END AS [GeoCodeTypeID],			
			nl.Notes,
			nl.CommonName
		FROM (
				--Home Location
				SELECT DISTINCT 
					 [CommonName] =C.commonName
					,[Address1] = C.Address1
					,[Address2] = [Address2]
					,[City]     = C.City
					,[State]    = C.State
					,[Zip]      = C.Zip			
					,[Latitude] = 0
					,[Longitude]= 0
		            ,[Notes]     = c.notes
				FROM  etl.locationImport  c
				WHERE [de].[Import_GetLocationID](c.CommonName,C.Address1,C.Address2,C.City,C.[State],C.Zip,0,0) IS NULL
				) NL

				 SET @RowCount = CONCAT(@RowCount,'[dbo].[Locations]: ', CAST(@@ROWCOUNT AS VARCHAR));


SELECT @RowCount,0,NULL; 

 COMMIT TRANSACTION
END TRY 
BEGIN CATCH
	IF @@ERROR > 0
	 ROLLBACK TRANSACTION;

	SELECT NULL,-1, CONCAT('ErrorLine: ',ERROR_LINE(),', Error: ',ERROR_MESSAGE());

END CATCH;