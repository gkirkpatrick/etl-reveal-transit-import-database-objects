﻿
--exec [ETL].[LoadCustomerImports_HRTPO_Customers_RAW_SP]

CREATE         
PROCEDURE [etl].[LoadCustomerImports_HRTPO_Customers_RAW_SP]
AS
SET NOCOUNT ON
/*********************************************************************
--* ======================================================================
--*  Author: Gene Kirkpatrick 
--*  Date Created: 6/2/2021
--*  VSS: 
--*=======================================================================
*
*	
*
*	Description: Import Customers Data from HRTPO Customers. Data is imported into [etl].[HRTPO_Customers_Raw] table
                
 .
*			
*	PARAMETERS: 
*
*	Change Log (Date, Name and Ddescription)
*
*	--------------------------------------------------------
*	Modification History: 
*********************************************************************/

 
DECLARE 
	@RowCount VARCHAR(255) = ''
	


BEGIN TRY
    BEGIN TRANSACTION

 INSERT INTO [ETL].[CustomerImport] ([CustomerImportID],[Description],[ImportedID],[BirthDate],[IsVerified]
             ,[CustomerNumber] ,[ProviderCustomerNumber] ,[Gender],[MedicalNumber],[Address1],[Address2]
             ,[City],[State],[Zip],[Latitude],[Longitude],[Cell Phone],[Home Phone],[Work Phone],[Email]
             ,[Language],[optInNightlyCallout],[OptInEmailNotifications],[optInVoiceNotifications]  
             ,[optinTextNotifications],[optinGeneralCallout],[OpDefaultResPassengerType],[OpDefaultResFundingType]
             ,[DefaultResCompanions],[DefaultResAttendants],[OpDefaultResService],[OpDefaultResFareType],[OpDefaultResOverrideType]        
             ,[First_Name],[Last_Name],[Middle_Name],[Extended_Info],[ETL_ImportFlag],Note )   
           
 
	SELECT

	   [CustomerImportID]           = [ClientID]
      ,[Description]				= CONCAT([LastName],', ',[FirstName])
      ,[ImportedID]					=  6
      ,[BirthDate]					= CASE WHEN ISDATE([DateOfBirth]) = 0 THEN '1/1/1800' ELSE [DateOfBirth] END
      ,[IsVerified]					= 0
      ,[CustomerNumber]				= 'N/A'   --ISNULL([PatientAccountNumber],'N/A')
      ,[ProviderCustomerNumber]		= NULL
      ,[Gender]					    = ISNULL(NULLIF([Gender],''),'N/A')
      ,[MedicalNumber]				= ''
      ,[Address1]					= CASE WHEN [StreetNo] IS NOT NULL THEN Concat([StreetNo],' ',[OnStreet]) ELSE [OnStreet] END
      ,[Address2]					= ISNULL([Unit],'') 
      ,[City]						= ISNULL([City],'N/A')
      ,[State]						= 'FL'                       --ISNULL([State],'N/A')
      ,[Zip]						= ISNULL([ZiPCode],'N/A')
      ,[Latitude]				    = '0.0'
      ,[Longitude]				    = '0.0'
      ,[Cell Phone]					=  NULL                      --NULLIF([CellNo],'')
      ,[Home Phone]					= NULLIF([Phone],'')
      ,[Work Phone]					= NULL                         -- NULLIF([WorkPhone],'')
      ,[Email]						=  ''                          -- CASE WHEN [EnableEmail] = 'Y' THEN ISNULL(Email,'') ELSE '' END
      ,[Language]					= 1                           --[Language]
      ,[optInNightlyCallout]        = 0 --CASE WHEN [EnableVoice] = 'Y' THEN 1 ELSE 0  END 
	  ,[OptInEmailNotifications]	= 0 --CASE WHEN [EnableEmail] = 'Y' THEN 1 ELSE 0 END
	  ,[optInVoiceNotifications]    = 0 --CASE WHEN [EnableVoice] = 'Y' THEN 1 ELSE 0  END                                 
	  ,[optinTextNotifications]     = 0
	  ,[optinGeneralCallout]		= 0 --CASE WHEN [EnableVoice] = 'Y' THEN 1 ELSE 0  END  
	  ,[OpDefaultResPassengerType]  =  CASE WHEN PrefSpaceType = 'AM' THEN 'AMBULATORY'
	                                        WHEN PrefSpaceType = 'BA' THEN 'BARIATRIC STRETCHER'
											WHEN PrefSpaceType = 'CS' THEN 'CAR SEAT'
											WHEN PrefSpaceType = 'SC' THEN 'SCOOTER'
											WHEN PrefSpaceType = 'ST' THEN 'STRETCHER'
											WHEN PrefSpaceType = 'WH' THEN 'WHEELCHAIR'
											WHEN PrefSpaceType = 'XA' THEN 'XW AMBULATORY'
											WHEN PrefSpaceType = 'XW' THEN 'EW WHEELCHAIR'
										
                                            ELSE 'AMBULATORY'  END
	  
	  
	 
	  ,[OpDefaultResFundingType]	= 'N/A'
	  ,[DefaultResCompanions]       =  CASE WHEN [AddDefaultPassType] = ' ' THEN 0 
	                                        WHEN [AddDefaultPassType] IS NULL THEN 0 
											WHEN [AddDefaultPassType] = 'PCA' THEN 0
											ELSE 1  END

	  ,[DefaultResAttendants]       =  CASE WHEN [AddDefaultPassType] = 'PCA' THEN 1 ELSE 0  END
	  ,[OpDefaultResService]        =  'PARATRANSIT - D2D'  
      ,[OpDefaultResFareType]       = 'N/A'
      ,[OpDefaultResOverrideType]   = 'N/A'
	  ,[First_Name]                 =  [FirstName]   
	  ,[Last_Name] 					=  [LastName] 
	  ,[Middle_Name]                =  [MiddleName] 
	  ,[Extended_Info]              =   CASE  WHEN LEFT([MobAids],2) = 'CN' THEN 'CANE'
											  WHEN LEFT([MobAids],2) = 'CR' THEN 'CRUTCHES'
											  WHEN LEFT([MobAids],2) = 'O2' THEN 'OXYGEN'
											  WHEN LEFT([MobAids],2) = 'RW' THEN 'ROLLING WALKER'
											  WHEN LEFT([MobAids],2) = 'SA' THEN 'SERVICE ANIMAL'
											  WHEN LEFT([MobAids],2) = 'VI' THEN 'VISUALLY IMPAIRED'
											  WHEN LEFT([MobAids],2) = 'WK' THEN 'WALKER'
	                                          ELSE NULL
											  END
	  ,[ETL_ImportFlag]             =  1
	  , [Note]                        = MobAids+' '+ AddComment
      
	  FROM [etl].[HRTPO_Customers_Raw]

  -- Set @RowCount for the last insert statement 
              SET @RowCount = CONCAT(@RowCount, ', [ETL].[CustomerImport] : ',  CAST(@@RowCount AS VARCHAR(255)));


SELECT @RowCount,0,NULL; 

 COMMIT TRANSACTION
END TRY 
BEGIN CATCH
	IF @@ERROR > 0
	 ROLLBACK TRANSACTION;

	SELECT NULL,-1, CONCAT('ErrorLine: ',ERROR_LINE(),', Error: ',ERROR_MESSAGE());

END CATCH;