﻿CREATE TABLE [etl].[HRTPO_Customers_Raw] (
    [ClientID]             VARCHAR (255)  NULL,
    [LastName]             VARCHAR (255)  NULL,
    [FirstName]            VARCHAR (255)  NULL,
    [MiddleName]           VARCHAR (255)  NULL,
    [DateofBirth]          VARCHAR (255)  NULL,
    [Gender]               VARCHAR (255)  NULL,
    [AddComment]           VARCHAR (2000) NULL,
    [PrefSpaceType]        VARCHAR (255)  NULL,
    [MobAids]              VARCHAR (255)  NULL,
    [AddDefaultPassType]   VARCHAR (255)  NULL,
    [DefaultFundingSource] VARCHAR (255)  NULL,
    [AddrName]             VARCHAR (255)  NULL,
    [StreetNo]             VARCHAR (255)  NULL,
    [OnStreet]             VARCHAR (255)  NULL,
    [Unit]                 VARCHAR (255)  NULL,
    [City]                 VARCHAR (255)  NULL,
    [ZipCode]              VARCHAR (255)  NULL,
    [Lat]                  VARCHAR (255)  NULL,
    [Lon]                  VARCHAR (255)  NULL,
    [Phone]                VARCHAR (255)  NULL
);

