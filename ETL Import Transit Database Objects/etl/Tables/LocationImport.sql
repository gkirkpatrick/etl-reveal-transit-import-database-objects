﻿CREATE TABLE [etl].[LocationImport] (
    [LocationImportID] INT           IDENTITY (1, 1) NOT NULL,
    [CommonName]       VARCHAR (255) NULL,
    [Address1]         VARCHAR (255) NULL,
    [Address2]         VARCHAR (255) NULL,
    [City]             VARCHAR (255) NULL,
    [State]            VARCHAR (255) NULL,
    [Zip]              VARCHAR (255) NULL,
    [Latitude]         VARCHAR (255) NULL,
    [Longitude]        VARCHAR (255) NULL,
    [Notes]            VARCHAR (255) NULL
);

