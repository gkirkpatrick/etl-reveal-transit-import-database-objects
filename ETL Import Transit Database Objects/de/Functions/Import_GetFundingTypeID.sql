﻿CREATE FUNCTION [de].[Import_GetFundingTypeID](
	@Description  varchar(255))
RETURNS int AS  
BEGIN 

Declare @FundingTypeID as int

Select top 1 @FundingTypeID = ft.FundingTypeID 
From FundingTypes ft 
Where ft.Description = @Description
Order by FundingTypeID desc

return @FundingTypeID

END