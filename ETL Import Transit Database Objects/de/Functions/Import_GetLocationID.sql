﻿


CREATE FUNCTION [de].[Import_GetLocationID](
	@CommonName varchar(255),
	@Address1 varchar(255),
	@Address2 varchar(255),
	@City varchar(255),
	@State varchar(255),
	@Zip varchar(255),
	@Latitude as decimal(18, 15),
	@Longitude as decimal(18, 15))
RETURNS int AS  
BEGIN 

Declare @LocationID as int

Declare @Description as Varchar(255)
Set @Description = (
	Case 
		When ISNULL(@CommonName,'') != '' 
			Then '[' + ISNULL(@CommonName,'') + '] '
		Else ''
	End + @Address1 + ' ' + 
	Case
		When ISNULL(@Address2,'') != ''
			Then ISNULL(@Address2,'') + ' '
		Else ''
	End + @City + ', ' + @State + ' ' + @Zip)


Select top 1 @LocationID = l.LocationID 
From Locations l 
Where l.ImportDescription = @Description and l.Active = 1 --and l.Latitude = @Latitude and l.Longitude = @Longitude
Order by LocationID desc


--If (@LocationID is null)
--Begin
--	Select top 1 @LocationID = l.LocationID 
--	From Locations l 
--	Where l.ImportDescription = @Description and l.OverrideImport = 1
--	Order by LocationID desc
--End


--If (@LocationID is null)
--Begin
--	Select top 1 @LocationID = l.LocationID 
--	From Locations l 
--	Where l.Latitude = @Latitude and l.Longitude = @Longitude and l.OverrideImport = 1 
--	Order by LocationID desc
--End


--If (@LocationID is null)
--Begin
--	Select top 1 @LocationID = l.LocationID 
--	From Locations l 
--	Where l.CommonName = @CommonName and l.Address1 = @Address1 and l.Address2 = @Address2 and l.City = @City and l.[State] = @State and l.Zip = @Zip
--	Order by LocationID desc
--End


return @LocationID
END