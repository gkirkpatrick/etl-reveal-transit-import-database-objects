﻿CREATE FUNCTION [de].[Import_GetPassengerTypeID](
	@Description varchar(255))
Returns int as
BEGIN

Declare @PassengerTypeID as int

Select Top 1 @PassengerTypeID = pt.PassengerTypeID 
From PassengerTypes pt 
Where pt.Description = @Description 
Order by PassengerTypeID desc

return @PassengerTypeID

END