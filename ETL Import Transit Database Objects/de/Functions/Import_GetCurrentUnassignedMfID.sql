﻿CREATE FUNCTION [de].[Import_GetCurrentUnassignedMfID](
	@Date as DateTime)
RETURNS int AS
BEGIN 

Declare @MfID as int

Declare @MfDataTypeID as int
Set @MfDataTypeID = (Select dbo.GetCurrentMfDataTypeID(@Date))

Select top 1 @MfID = m.MfID
From Mfs m inner join MfDatas md on md.MfDataID = m.MfDataID
Where m.[Description] = 'Unassigned' and m.[Date] = @Date and md.MfDataTypeID = @MfDataTypeID and m.Active = 1
Order by m.MfID desc

return @MfID

END