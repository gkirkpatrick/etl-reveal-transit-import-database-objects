﻿CREATE FUNCTION [de].[Import_GetOverrideTypeID](
	@Description  varchar(255))
RETURNS int AS  
BEGIN 

Declare @OverrideTypeID as int

Select top 1 @OverrideTypeID = ot.OverrideTypeID 
From OverrideTypes ot
Where ot.Description = @Description
Order by OverrideTypeID desc

return isnull(@OverrideTypeID,1)

END