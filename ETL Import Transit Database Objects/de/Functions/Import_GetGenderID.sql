﻿
CREATE FUNCTION [de].[Import_GetGenderID](
	@Description varchar(255))
RETURNS int AS
BEGIN 

Declare @GenderID as int

Select top 1 @GenderID = g.GenderID 
From Genders g 
Where RTRIM(LTRIM(g.Description)) = RTRIM(LTRIM(@Description)) 
Order by g.GenderID desc

return @GenderID

END