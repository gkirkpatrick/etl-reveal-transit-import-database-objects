﻿
CREATE FUNCTION [de].[Import_GetCancelReasonID](
	@CancelReason varchar(255))
RETURNS int AS  
BEGIN 

Declare @CancelReasonID as int

Select top 1 @CancelReasonID = c.CancelReasonID 
From CancelReasons c
Where c.Description = @CancelReason 
Order by CancelReasonID desc

return @CancelReasonID

END