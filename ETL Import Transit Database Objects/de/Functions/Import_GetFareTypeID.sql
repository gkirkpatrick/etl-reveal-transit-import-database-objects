﻿


CREATE     FUNCTION [de].[Import_GetFareTypeID](
	@Description  varchar(255))
RETURNS int AS  
BEGIN 

Declare @FaretypeID as int

Select top 1 @FareTypeID = f.FareTypeID 
From FareTypes f 
Where f.Description = @Description
Order by FareTypeID desc

return @FareTypeID

END