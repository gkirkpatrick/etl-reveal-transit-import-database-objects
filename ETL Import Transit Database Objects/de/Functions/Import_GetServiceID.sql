﻿CREATE FUNCTION [de].[Import_GetServiceID](
	@Description  varchar(255))
RETURNS int AS  
BEGIN 

Declare @ServiceID as int

Select top 1 @ServiceID = s.ServiceID 
From Services s 
Where s.Description = @Description
Order by ServiceID desc

return @ServiceID

END