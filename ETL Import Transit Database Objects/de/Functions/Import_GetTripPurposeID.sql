﻿CREATE FUNCTION [de].[Import_GetTripPurposeID](
	@Description  varchar(255))
RETURNS int AS  
BEGIN 

Declare @TripPurposeID as int

Select top 1 @TripPurposeID = tp.TripPurposeID 
From TripPurposes tp 
Where tp.Description = @Description or tp.ImportID = @Description
Order by TripPurposeID desc

return @TripPurposeID

END