﻿
CREATE FUNCTION [de].[Import_GetCustomerID](
	@Description varchar(255),
	@BirthDate as datetime, 
	@CustomerNumber as varchar(255))
RETURNS int AS
BEGIN 

Declare @CustomerID as int

Select top 1 @CustomerID = c.CustomerID 
From Customers c 
Where c.Description=  @Description and c.BirthDate = isnull(@BirthDate,'1/1/1800') and c.CustomerNumber = @CustomerNumber
Order by CustomerID desc

return @CustomerID

END