﻿
CREATE FUNCTION [de].[Import_GetProviderID](
	@ProviderCode  varchar(255))
RETURNS int AS  
BEGIN 

Declare @ProviderID as int

Select top 1 @ProviderID = pc.ProviderID 
From ProviderCodes pc
Where pc.ProviderCode = @ProviderCode
Order by ProviderID desc

return @ProviderID

END