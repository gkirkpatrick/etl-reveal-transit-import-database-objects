﻿

CREATE FUNCTION [de].[Import_GetCurrentMfID](
	@Date as DateTime,
	@ProviderTripID as Varchar(255))
RETURNS int AS
BEGIN 

Declare @MfID as int

Declare @MfDataTypeID as int
Set @MfDataTypeID = (Select dbo.GetCurrentMfDataTypeID(@Date))

Select top 1 @MfID = m.MfID
From MfTrips mt inner join Mfs m on m.MfID = mt.MfID and m.Date = @Date inner join MfDatas md on md.MfDataID = m.MfDataID
Where mt.ProviderTripID = @ProviderTripID and md.MfDataTypeID = @MfDataTypeID and m.Active = 1
Order by m.MfID desc

return @MfID

END