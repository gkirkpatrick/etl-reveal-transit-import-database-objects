﻿

CREATE FUNCTION [de].[Import_GetCurrentMfTripID](
	@Date as DateTime,
	@ProviderTripID as Varchar(MAX))
RETURNS int AS  
BEGIN 

--Declare @ProviderTripID as Varchar(MAX)
--Declare @Date as DateTime

--set @ProviderTripID = 'WFLA1700527A'
--set @Date = '8/28/2017'

Declare @MfTripID as int

Select top 1 @MfTripID = mt.MfTripID
From MfTrips mt inner join Mfs m on m.MfID = mt.MfID inner join MfDatas md on md.MfDataID = m.MfDataID
Where mt.ProviderTripID = @ProviderTripID and m.Date = @Date and m.Active = 1 and md.MfDataTypeID = dbo.GetCurrentMfDataTypeID(@Date)
Order by mt.MfTripID desc

return @MfTripID

END