﻿

CREATE     FUNCTION [de].[Import_GetSystemTripNoteID](
	@Description  varchar(255))
RETURNS int AS  
BEGIN 

Declare @SystemTripNoteID as int

Select top 1 @SystemTripNoteID = S.SystemTripNoteID 
From  dbo.SystemTripNotes S 
Where S.Description = @Description
Order by SystemTripNoteID desc

return @SystemTripNoteID
end